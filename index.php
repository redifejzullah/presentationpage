<!DOCTYPE html>
<html lang="it">
<head>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="css/styles.css">
  <title>Presentazione MNP</title>
</head>
<body>
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center ">
        <p>Nuovo sito mnp</p>
      </div>
      <div class="col-sm-4 text-center">
        One of three columns
        <div class="img1container">
          <img src="img/img1.jpg" class="rounded text-left img" alt="Responsive image">
        </div>
        <div class="img2container">
          <img src="img/img2.jpg" class="rounded text-right img" alt="Responsive image">
        </div>
        <div class="img3container">
          <img src="img/img3.jpg" class="rounded text-left img" alt="Responsive image">
        </div>
      </div>
      <div class="col-sm-4 text-center">
        One of three columns
      </div>
      <div class="col-sm-4 text-center">
        One of three columns
      </div>
    </div>
  </div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
